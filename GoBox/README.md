# GoBox Constellation
![Hardware Photo](Example.jpg)
A suite of flexible stackable and interlocking storage boxes that can also be used as modular furniture. Designed with self finishing in mind, especially to get the perfect match with the installation's environment. Designed and prototyped on a modular grid so that they are interchangeable and allow you to choose your own patterning and layout. 

## The constellations boxes are:-

### Full Depth, Standard
![Full Depth, Standard, Photo](Assembly-FD-Std.jpg)

Comprises the following parts:-

1.  Back-FD-Std.fcstd
2.  Base-FD-Std.fcstd
3.  LHS-FD-Std.fcstd
4.  Top-FD-Std.FCStd
5.  RHS-FD-Std.fcstd

### Full Depth, Double
![Full Depth, Double, Photo](Assembly-FD-Dbl.jpg)

Comprises the following parts:-

1.  Back-FD-Dbl.fcstd
2.  Base-FD-Dbl.fcstd
3.  LHS-FD-Std.fcstd
4.  Top-FD-Dbl.fcstd
5.  RHS-FD-Std.fcstd

### Full Depth, Half Left
![Full Depth, Half Left, Photo](Assembly-FD-HlfL.jpg)

Comprises the following parts:-

1.  Back-FD-Hlf.fcstd
2.  Base-FD-Hlf.fcstd
3.  LHS-FD-HlfL.fcstd
4.  Top-FD-HlfL.fcstd
5.  RHS-FD-Std.fcstd

### Full Depth, Half Right
![Full Depth, Half Right, Photo](Assembly-FD-HlfR.jpg)

Comprises the following parts:-

1.  Back-FD-Hlf.fcstd
2.  Base-FD-Hlf.fcstd
3.  LHS-FD-Std.fcstd
4.  Top-FD-HlfR.fcstd
5.  RHS-FD-HlfR.fcstd

### Half Depth, Standard
![Half Depth, Standard, Photo](Assembly-HD-Std.jpg)

Comprises the following parts:-

1.  Back-HD-Std.fcstd
2.  Base-HD-Std.fcstd
3.  LHS-HD-Std.fcstd
4.  Top-HD-Std.fcstd
5.  RHS-HD-Std.fcstd

### Half Depth, Double
![Half Depth, Double, Photo](Assembly-HD-Dbl.jpg)

Comprises the following parts:-

1.  Back-HD-Dbl.fcstd
2.  Base-HD-Dbl.fcstd
3.  LHS-HD-Std.fcstd
4.  Top-HD-Dbl.fcstd
5.  RHS-HD-Std.fcstd

### Half Depth, Half Left
Work in progress.

### Half Depth, Half Right
Work in progress.

## Project Collaborators
Tweaks to original design, hand built prototypes and additional boxes added to extend the concept beyond the standard, have been  contributed by [AJ Ibbotson](https://aaronjibbotson.wordpress.com/). Prototypes were built, and are displayed, in use, at [Sheffield Hackspace](http://www.sheffieldhardwarehackers.org.uk/wordpress/).

[AL's GoBox Build Images](https://www.flickr.com/photos/138946228@N06/sets/72157665874643632/with/26185997712/)

[AJ's Blog and GoBox article](https://aaronjibbotson.wordpress.com/2016/04/17/gobox-modular-storage-system/)

[AJ's Twitter Feed](https://twitter.com/AJ_Ibbo)

[Sheffield Hackspace's Web Home](http://www.sheffieldhardwarehackers.org.uk/wordpress/)

[Sheffield Hackspace's Twitter Feed](https://twitter.com/shhmakers)

## Notes:-
1. Features storage space sized for the popular 35 Litre "Really Useful" storage box.
2. May be used in conjunction with the LokBox lockable storage box routable project which is sized to fit in a GoBox.
3. Left, Right etc. are as presented when you are stood facing the opening of a GoBox.

