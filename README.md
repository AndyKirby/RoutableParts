# RoutableParts

Designs for projects created with CNC routing in mind. All design work was created using Freecad.

## Projects:-
* GoBox, Flexible stackable interloking storage boxes. Features integral storage space sized for a popular 35 Litre useful storage box.
* LokBox, Lockable flexible box for use in GoBox.
* LatheTBox, A toolbox for my mini lathe.

